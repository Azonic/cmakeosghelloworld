Minimal implementation for education purpose of CMake and OpenScenegraph with a few basic geometry operations on VertexBufferObjects (VBOs)

Requirements:
- Visual Studio (tested with 2017)
- CMake
- OpenScenegraph binaries: http://mirevi.de/claymore_deps/osg-3640.zip)

You'll need some binaries next to the CMakeOSGHelloWorld.exe. May be useful:
http://www.dependencywalker.com/